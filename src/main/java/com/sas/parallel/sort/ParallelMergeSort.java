package com.sas.parallel.sort;

import java.util.LinkedList;
import java.util.Queue;

/**
 * "Merge sort." Wikipedia. Wikimedia Foundation.
 */
public class ParallelMergeSort {

    /**
     * Split the array in half recursively while continuously merging it in parallel.
     *
     * @param array Array to sort
     * @param low   The lowest index to sort
     * @param high  The highest index to sort
     */
    public static void sort(final long[] array, final int low, final int high) {
        final ParallelMergeSort parallelMergeSort = new ParallelMergeSort();
        final Thread thread = parallelMergeSort.fork(array, low, high);

        try {
            thread.join();
        } catch(final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create another thread to sort another segment of the array.
     * @param array Array to sort
     * @param low The lowest index to sort
     * @param high The highest index to sort
     * @return The sort thread
     */
    public Thread fork(final long[] array, final int low, final int high) {
        final MergeSortRunnable runnable = new MergeSortRunnable(array, low, high);

        final Thread thread = new Thread(runnable, "Sub-Array <" + low + "," + high + ">");
        thread.start();

        return thread;
    }

    /**
     * A threadable implementation of the merge sort algorithm.
     */
    private class MergeSortRunnable implements Runnable {
        final long[] array;
        final int low;
        final int high;

        MergeSortRunnable(final long[] array, final int low, final int high) {
            this.array = array;
            this.low = low;
            this.high = high;
        }

        @Override
        public void run() {
            if (low < high) {
                final int middle = (low + high) / 2;

                final Thread lowThread = fork(array, low, middle);
                final Thread highThread = fork(array, middle + 1, high);

                try {
                    // block this thread until both child threads are done.
                    lowThread.join();
                    highThread.join();
                } catch(final Exception e) {
                    e.printStackTrace();
                }

                merge(array, low, middle, high);
            }
        }

        /**
         * Sort and merge values back into the array.  Copy sub-arrays to separate queues and then merge back into the
         * original array.
         *
         * @param array  The array to be sorted
         * @param low    The lowest index
         * @param middle The middle index
         * @param high   The highest index
         */
        private void merge(final long[] array, final int low, final int middle, final int high) {
            int i; // counter
            final Queue<Long> left = new LinkedList<>();
            final Queue<Long> right = new LinkedList<>();

            // Copy left half of array into a queue
            for (int j = low; j <= middle; j++) {
                left.offer(array[j]);
            }

            // Copy right half of array into a queue
            for (int j = middle + 1; j <= high; j++) {
                right.offer(array[j]);
            }

            i = low;

            // Loop over the queues until 1 is empty
            while (!(left.isEmpty() || right.isEmpty())) {
                // Compare and copy back to the original array
                if (left.peek() <= right.peek()) {
                    array[i++] = left.poll();
                } else {
                    array[i++] = right.poll();
                }
            }

            /*
               Copy left over values back into the array.  This accounts for an odd number of items in the array.  Only
               1 of the 2 loops may actually have any value left.
             */

            while (!left.isEmpty()) {
                array[i++] = left.poll();
            }

            while (!right.isEmpty()) {
                array[i++] = right.poll();
            }

//            final String threadName = Thread.currentThread().getName();
//            System.out.println("Finished processing: " + threadName);
        }
    }
}
