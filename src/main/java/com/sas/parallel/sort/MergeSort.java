package com.sas.parallel.sort;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Skiena, Steven S.  The Algorithm Design Manual, 2010. pp 122-123.
 * <p>
 * Divide and conquer sort algorithm.
 */
public class MergeSort {

    /**
     * Split the array in half recursively while continuously merging it.
     *
     * @param array Array to sort
     * @param low   The lowest index to sort
     * @param high  The highest index to sort
     */
    public static void sort(final long[] array, final int low, final int high) {
        if (low < high) {
            final int middle = (low + high) / 2;
            sort(array, low, middle);
            sort(array, middle + 1, high);
            merge(array, low, middle, high);
        }
    }

    /**
     * Sort and merge values back into the array.  Copy sub-arrays to separate queues and then merge back into the
     * original array.
     *
     * @param array  The array to sort
     * @param low    The lowest index
     * @param middle The middle index
     * @param high   The highest index
     */
    private static void merge(final long[] array, final int low, final int middle, final int high) {
        int i; // counter
        final Queue<Long> left = new LinkedList<>();
        final Queue<Long> right = new LinkedList<>();

        // Copy left half of array into a queue
        for (int j = low; j <= middle; j++) {
            left.offer(array[j]);
        }

        // Copy right half of array into a queue
        for (int j = middle + 1; j <= high; j++) {
            right.offer(array[j]);
        }

        i = low;

        // Loop over the queues until 1 is empty
        while (!(left.isEmpty() || right.isEmpty())) {
            // Compare and copy back to the original array
            if (left.peek() <= right.peek()) {
                array[i++] = left.poll();
            } else {
                array[i++] = right.poll();
            }
        }

        /*
           Copy left over values back into the array.  This accounts for an odd number of items in the array.  Only
           1 of the 2 loops may actually have any value left.
         */

        while (!left.isEmpty()) {
            array[i++] = left.poll();
        }

        while (!right.isEmpty()) {
            array[i++] = right.poll();
        }

        // System.out.println("Finished processing: Sub-Array <" + low + "," + high + ">");
    }
}
