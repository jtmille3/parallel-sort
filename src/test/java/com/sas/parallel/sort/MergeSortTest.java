package com.sas.parallel.sort;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MergeSortTest {

    private static long master[];
    private long array[];

    @BeforeClass
    public static void beforeClass() {
        MergeSortTest.master = Tools.generateRandomizedArray(5, 10);
    }

    @Before
    public void setup() {
        if(array == null) {
            array = new long[master.length];
        }
        System.arraycopy(master, 0, array, 0, master.length);
    }

    private void testForSortedArray(final long[] array) {
        for(int i = 0; i < array.length; i++) {
            if(i + 1 == array.length) {
                break;
            }

            Assert.assertTrue(array[i] <= array[i + 1]);
        }
    }

    @Test
    public void testMergeSortSerial() {
        Tools.print(array);

        final long milliseconds = System.currentTimeMillis();

        final int low = 0;
        final int high = array.length - 1;
        MergeSort.sort(array, low, high);

        System.out.println(Metrics.interval(milliseconds));

        Tools.print(array);

        testForSortedArray(array);
    }

    @Test
    public void testMergeSortParallel() {
        Tools.print(array);

        final long milliseconds = System.currentTimeMillis();

        final int low = 0;
        final int high = array.length - 1;
        ParallelMergeSort.sort(array, low, high);

        System.out.println(Metrics.interval(milliseconds));

        Tools.print(array);

        testForSortedArray(array);
    }
}
