Question #1: Java code: Use multiple threads to sort an array of numbers, send code to discuss
- Please bring your completed program on a USB drive and we will make available a computer to view the file(s). 
- Please be prepared to discuss your program and how you derived the solution.

Question #2: Essay question: Given N elements and K threads, what is theoretical big O of algorithm? is it optimal?
- Please write a response and bring it with you.